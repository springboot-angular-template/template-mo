# Template Spring Boot / JWT

### Application Spring Boot prennant en charge l'authentification basée sur des jetons JWT

1. Documentation des endpoints



| MÉTHODE | URL              | ACTIONS                                |
|---------|------------------|----------------------------------------|
| POST    | /api/auth/signup | créer un nouveau compte                |
| POST    | /api/auth/signin | se connecter à un compte               |
| GET     | /api/test/all    | récupérer du contenu public            |
| GET     | /api/test/user   | accéder au contenu de l'utilisateur    |
| GET     | /api/test/mod    | accéder au contenu du modérateur       |
| GET     | /api/test/admin  | accéder au contenu de l'administrateur |


2. Architecture simplifiée du projet

```
src
└── main
    ├── java
    │   └── com.kz.templatetoken
    │       ├── controllers
    │       │   ├── AuthController.java
    │       │   └── TestController.java
    │       ├── models
    │       │   ├── Role.java
    │       │   ├── RolesEnum.java
    │       │   └── User.java
    │       ├── payload
    │       │   ├── request
    │       │   │   ├── LoginRequest.java
    │       │   │   └── SignupRequest.java
    │       │   └── response
    │       │       ├── JwtResponse.java
    │       │       └── MessageResponse.java
    │       ├── repository
    │       │   ├── RoleRepository.java
    │       │   └── UserRepository.java
    │       ├── security
    │       │   ├── jwt
    │       │   │   ├── AuthEntryPointJwt.java
    │       │   │   ├── AuthTokenFilter.java
    │       │   │   └── JwtUtils.java
    │       │   ├── services
    │       │   │   ├── UserDetailsImpl.java
    │       │   │   └── UserDetailsService.java
    │       │   └── WebSecurityConfig.java
    │       └── TemplateTokenApplication.java
    └── resources
        └── application.yml
```