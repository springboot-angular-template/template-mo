CREATE TABLE role
(
    id   BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    CONSTRAINT role_id_uq UNIQUE (id),
    CONSTRAINT role_name_uq UNIQUE (name)
);

CREATE TABLE user
(
    id       BIGINT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(20)  NOT NULL,
    email    VARCHAR(50)  NOT NULL,
    password VARCHAR(120) NOT NULL,
    CONSTRAINT user_email_uq UNIQUE (email),
    CONSTRAINT user_id_uq UNIQUE (id),
    CONSTRAINT user_username_uq UNIQUE (username)
);

CREATE TABLE user_role_assoc
(
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, role_id),
    CONSTRAINT role_id_fk FOREIGN KEY (role_id) REFERENCES role (id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES user (id)
);


INSERT INTO role (id, name) VALUES (3, 'ROLE_ADMIN');
INSERT INTO role (id, name) VALUES (2, 'ROLE_MODERATOR');
INSERT INTO role (id, name) VALUES (1, 'ROLE_USER');

INSERT INTO user (id, username, email, password) VALUES (1, 'user', 'user@gmail.com', '$2a$10$ni.ikPKRFmnP/HBAYGuXd.1yGTlKKaD0FDDbU0ulXbYRBMB5NUvU6');
INSERT INTO user (id, username, email, password) VALUES (2, 'moderator', 'moderator@gmail.com', '$2a$10$SRtpPVkmdFduKM5w9F5rwOxZCvIsYyN1O6HuyFVCqkgP21llJKoCO');
INSERT INTO user (id, username, email, password) VALUES (3, 'admin', 'admin@gmail.com', '$2a$10$B4I7mn15SJ8YfSaiRG2rveV0nwChqYufEw6yNnAmbe34zKlMkvZ/2');
INSERT INTO user (id, username, email, password) VALUES (4, 'super_admin', 'super_admin@gmail.com', '$2a$10$/Du0h.EXvNK2wdjhuJyzxOl9uBnPWZBMoMTsT7WZhXk.c3LS2JFLy');

INSERT INTO user_role_assoc (user_id, role_id) VALUES (1, 1);
INSERT INTO user_role_assoc (user_id, role_id) VALUES (4, 1);
INSERT INTO user_role_assoc (user_id, role_id) VALUES (2, 2);
INSERT INTO user_role_assoc (user_id, role_id) VALUES (4, 2);
INSERT INTO user_role_assoc (user_id, role_id) VALUES (3, 3);
INSERT INTO user_role_assoc (user_id, role_id) VALUES (4, 3);
