package com.kz.templatetoken.repository;

import com.kz.templatetoken.models.Role;
import com.kz.templatetoken.models.RolesEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(RolesEnum name);

}