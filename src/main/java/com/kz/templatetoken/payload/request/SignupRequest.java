package com.kz.templatetoken.payload.request;

import com.kz.templatetoken.models.RolesEnum;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

import static javax.persistence.EnumType.STRING;

@Data
@Builder
public class SignupRequest {

    @NotBlank
    private String username;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    private String password;

    @NotEmpty
    @Enumerated(STRING)
    private Set<RolesEnum> roles;

}
