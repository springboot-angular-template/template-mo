package com.kz.templatetoken.models;

public enum RolesEnum {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
