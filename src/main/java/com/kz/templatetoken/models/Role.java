package com.kz.templatetoken.models;

import lombok.*;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(STRING)
    @Column(unique = true, nullable = false, length = 20)
    private RolesEnum name;

}
